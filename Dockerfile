# 构建前端项目
FROM node:18 as frontend-builder

WORKDIR /usr/src/app

COPY ./chat-master-web /usr/src/app

RUN npm install
RUN npm install whatwg-fetch --save
RUN npm run build

# 构建管理端项目
FROM node:14 as admin-builder

WORKDIR /usr/src/app

COPY ./chat-master-admin /usr/src/app

RUN npm install
RUN npm run build:prod

# 将前端和管理端打包后的文件合并
FROM caddy:latest

# 将前端和管理端构建后的文件复制到caddy容器中
COPY --from=frontend-builder /usr/src/app/dist /usr/share/caddy
COPY --from=admin-builder /usr/src/app/dist /usr/share/caddy/admin

# 复制Caddyfile到caddy容器中
COPY Caddyfile /etc/caddy/Caddyfile

# 设置日志目录映射
VOLUME /var/log/caddy

# 开放80和443端口
EXPOSE 80
EXPOSE 443
