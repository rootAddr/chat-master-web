
## 介绍
### docker一键部署chat-master前后端面板

基于[原作者地址](https://gitee.com/panday94/chat-master)修改和优化

需要先配置服务端才能正常运行：[点我查看服务端部署教程](https://gitee.com/wanfeng789/chat-master)

#### 下载项目
```
cd /home
```
```
git clone https://gitee.com/wanfeng789/chat-master-web.git
```
```
cd chat-master-web
```
#### 修改`Caddyfile`第一行配置替换为你的域名

#### 一键运行
```
docker compose up -d
```

#### 使用

域名会自动开启`https`，路径`/admin`是管理后台

默认管理员账户：`admin`      密码：`123456`



[点我查看服务端部署教程](https://gitee.com/wanfeng789/chat-master)